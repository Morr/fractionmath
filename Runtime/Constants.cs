﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

namespace Fractions
{
    /// <summary>
    /// FractionMath constants
    /// </summary>
    public static class Constants
    {
        public const int DENOMINATOR = 1000;

        public const int FRACTION_PART_DITIGS = 3;
        public const string FRACTION_PART_FORMAT = "D3";
    }
}

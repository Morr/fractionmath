﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

namespace Fractions
{
    public static partial class FractionMath
    {
        public static Fraction Abs(Fraction f)
        {
            return new Fraction(math.abs(f.Numerator));
        }

        public static Fraction Sign(Fraction f)
        {
            return f.Numerator < 0 ? -Fraction.One : Fraction.One;
        }

        public static Fraction Sin(Fraction angle)
        {
            return SinLut[angle.Numerator];
        }

        public static Fraction Cos(Fraction angle)
        {
            return CosLut[angle.Numerator];
        }

        public static Fraction Tan(Fraction angle)
        {
            return TanLut[angle.Numerator];
        }
    }
}


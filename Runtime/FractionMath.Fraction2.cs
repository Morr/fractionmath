﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

namespace Fractions
{
    public static partial class FractionMath
    {
        public static Fraction SqrDistance(Fraction2 v1, Fraction2 v2)
        {
            Fraction2 difference = v2 - v1;
            return (difference.x * difference.x) + (difference.y * difference.y);
        }

        public static Fraction Dot(Fraction2 v1, Fraction2 v2)
        {
            return (v1.x * v2.x) + (v1.y * v2.y);
        }

        public static Fraction Magnitude(Fraction2 vector)
        {
            if (TryGetLookupIndex(vector, out int lookupIndex, out Fraction divisor) == true)
            {
                return RectLengths[lookupIndex] * divisor;
            }

            return Fraction.Zero;
        }

        public static Fraction2 Normalize(Fraction2 vector)
        {
            if (TryGetLookupIndex(vector, out int lookupIndex, out Fraction divisor) == true)
            {
                Fraction2 normalized = FractionMath.RectNormalizedVectors[lookupIndex];
                Fraction normalizedX = vector.x < 0 ? -normalized.x : normalized.x;
                Fraction normalizedY = vector.y < 0 ? -normalized.y : normalized.y;
                return new Fraction2(normalizedX, normalizedY);
            }

            return new Fraction2(Fraction.Zero, Fraction.One);
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

namespace Fractions
{
    public static partial class FractionMath
    {
        public static Fraction LoopAngle(Fraction angle)
        {
            int modulo = angle.Numerator % Constants.DENOMINATOR;

            if (angle > 0)
            {
                return new Fraction(modulo);
            }
            else if (angle < 0)
            {
                return new Fraction(modulo + Constants.DENOMINATOR);
            }

            return angle;
        }

        public static Fraction AbsoluteAngle(Fraction2 vector)
        {
            if (TryGetLookupIndex(vector, out int lookupIndex, out Fraction divisor) == true)
            {
                if (vector.x > 0)
                {
                    if (vector.y > 0)
                    {
                        return RectAngles[lookupIndex];
                    }
                    else
                    {
                        return Fraction.Half - RectAngles[lookupIndex];
                    }
                }
                else
                {
                    if (vector.y > 0)
                    {
                        return Fraction.One - RectAngles[lookupIndex];
                    }
                    else
                    {
                        return Fraction.Half + RectAngles[lookupIndex];
                    }
                }
            }

            return Fraction.Zero;
        }

        public static Fraction SignedAngle(Fraction2 from, Fraction2 to)
        {
            Fraction fromAbsoluteAngle = AbsoluteAngle(from);
            Fraction toAbsoluteAngle = AbsoluteAngle(to);
            Fraction difference = toAbsoluteAngle - fromAbsoluteAngle;
            Fraction absoluteDifference = Abs(difference);

            if (absoluteDifference > Fraction.Half)
            {
                Fraction oneMinusDifference = Fraction.One - absoluteDifference;
                difference = difference.Numerator < 0 ? oneMinusDifference : -oneMinusDifference;
            }

            return difference;
        }

        public static Fraction2 CalculateDirectionVector(Fraction absoluteAngle)
        {
            return AngleToDirectionLut[absoluteAngle.Numerator];
        }
    }
}


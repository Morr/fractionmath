using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Fractions
{
    [System.Serializable]
    public struct Fraction
    {
        public static readonly Fraction Zero = new Fraction(0);
        public static readonly Fraction Quarter = new Fraction(1, 4);
        public static readonly Fraction Half = new Fraction(1, 2);
        public static readonly Fraction ThreeQuarters = new Fraction(3, 4);
        public static readonly Fraction One = new Fraction(1, 1);

        public Int32 Numerator;

        public Fraction(int numerator)
        {
            Numerator = numerator;
        }

        public Fraction(int numerator, int denominator)
        {
            long result = numerator;
            result *= Constants.DENOMINATOR;
            result /= denominator;
            Numerator = (Int32)result;
        }

        public static bool operator ==(Fraction f1, Fraction f2)
        {
            return f1.Numerator == f2.Numerator;
        }

        public static bool operator !=(Fraction f1, Fraction f2)
        {
            return f1.Numerator != f2.Numerator;
        }

        public static bool operator >(Fraction f1, Fraction f2)
        {
            return f1.Numerator > f2.Numerator;
        }

        public static bool operator <(Fraction f1, Fraction f2)
        {
            return f1.Numerator < f2.Numerator;
        }

        public static bool operator >=(Fraction f1, Fraction f2)
        {
            return f1.Numerator >= f2.Numerator;
        }

        public static bool operator <=(Fraction f1, Fraction f2)
        {
            return f1.Numerator <= f2.Numerator;
        }

        public override bool Equals(object obj)
        {
            return obj is Fraction fraction && Numerator == fraction.Numerator;
        }

        public override int GetHashCode()
        {
            return Numerator.GetHashCode();
        }

        public static Fraction operator +(Fraction f)
        {
            return f;
        }

        public static Fraction operator -(Fraction f)
        {
            return new Fraction(-f.Numerator);
        }

        public static Fraction operator +(Fraction f1, Fraction f2)
        {
            return new Fraction(f1.Numerator + f2.Numerator);
        }

        public static Fraction operator -(Fraction f1, Fraction f2)
        {
            return new Fraction(f1.Numerator - f2.Numerator);
        }

        public static Fraction operator *(Fraction f1, Fraction f2)
        {
            long result = f1.Numerator;
            result *= f2.Numerator;
            result /= Constants.DENOMINATOR;
            return new Fraction((Int32)result);
        }

        public static Fraction operator /(Fraction f1, Fraction f2)
        {
            if (f2.Numerator == 0)
            {
                throw new System.DivideByZeroException();
            }

            long result = f1.Numerator;
            result *= Constants.DENOMINATOR;
            result /= f2.Numerator;
            return new Fraction((Int32)result);
        }

        public static implicit operator Fraction(int integer)
        {
            return new Fraction(integer * Constants.DENOMINATOR);
        }

        public static explicit operator float(Fraction f)
        {
            return ((float)f.Numerator) / Constants.DENOMINATOR;
        }

        public static explicit operator Fraction(float f)
        {
            int numerator = (int)(Constants.DENOMINATOR * f);
            return new Fraction(numerator);
        }

        public override string ToString()
        {
            int fractionPart = math.abs(Numerator % Constants.DENOMINATOR);
            int wholePart = (Numerator - fractionPart) / Constants.DENOMINATOR;
            return ($"{wholePart}.{fractionPart.ToString(Constants.FRACTION_PART_FORMAT)}");
        }

        public static bool TryParse(string text, out Fraction number)
        {
            try
            {
                text = text.Replace(",", ".");
                string[] parts = text.Split('.');

                // Whole part
                int numerator = 0;
                int wholePart = 0;

                if (parts[0].Length > 0)
                {
                    wholePart += int.Parse(parts[0]) * Constants.DENOMINATOR;
                }

                numerator += wholePart;

                // Fraction part (first three digits)
                if (parts.Length > 1)
                {
                    int charactersToAdd = math.max((Constants.FRACTION_PART_DITIGS - parts[1].Length), 0);
                    parts[1] = charactersToAdd > 0 ? parts[1] + new string('0', charactersToAdd) : parts[1].Substring(0, Constants.FRACTION_PART_DITIGS);
                    int fractionPart = math.abs(int.Parse(parts[1]));

                    if (wholePart < 0)
                    {
                        fractionPart = -fractionPart;
                    }

                    int rest = fractionPart % Constants.DENOMINATOR;
                    numerator += (fractionPart - rest) * Constants.DENOMINATOR;
                    numerator += rest;
                }

                number = new Fraction(numerator);
                return true;
            }
            catch
            {
                number = Fraction.Zero;
                return false;
            }
        }
    }
}

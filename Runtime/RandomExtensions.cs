using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Fractions
{
    public static class RandomExtensions
    {
        public static Fraction NextFraction(this ref Unity.Mathematics.Random random, Fraction min, Fraction max)
        {
            int numerator = random.NextInt(min.Numerator, max.Numerator);
            return new Fraction(numerator);
        }

        public static Fraction2 NextFraction2(this ref Unity.Mathematics.Random random, Fraction2 min, Fraction2 max)
        {
            int2 intMin = new int2(min.x.Numerator, min.y.Numerator);
            int2 intMax = new int2(max.x.Numerator, max.y.Numerator);
            int2 randomNumerators = random.NextInt2(intMin, intMax);
            return new Fraction2(new Fraction(randomNumerators.x), new Fraction(randomNumerators.y));
        }
    }
}

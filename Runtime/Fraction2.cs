using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

namespace Fractions
{
    [System.Serializable]
    public struct Fraction2
    {
        public static readonly Fraction2 Zero = new Fraction2(new Fraction(0), new Fraction(0));

        public Fraction x;
        public Fraction y;

        public static object MathOperations { get; private set; }

        public Fraction2(Fraction x, Fraction y)
        {
            this.x = x;
            this.y = y;
        }

        public static bool operator ==(Fraction2 v1, Fraction2 v2)
        {
            return v1.x == v2.x && v1.y == v2.y;
        }

        public static bool operator !=(Fraction2 v1, Fraction2 v2)
        {
            return v1.x != v2.x || v1.y != v2.y;
        }

        public override bool Equals(object obj)
        {
            return obj is Fraction2 fraction && x == fraction.x && y == fraction.y;
        }

        public override int GetHashCode()
        {
            int hashCode = 1502939027;
            hashCode = hashCode * -1521134295 + x.GetHashCode();
            hashCode = hashCode * -1521134295 + y.GetHashCode();
            return hashCode;
        }

        public static Fraction2 operator +(Fraction2 v)
        {
            return v;
        }

        public static Fraction2 operator -(Fraction2 v)
        {
            return new Fraction2(-v.x, -v.y);
        }

        public static Fraction2 operator +(Fraction2 v1, Fraction2 v2)
        {
            return new Fraction2(v1.x + v2.x, v1.y + v2.y);
        }

        public static Fraction2 operator -(Fraction2 v1, Fraction2 v2)
        {
            return new Fraction2(v1.x - v2.x, v1.y - v2.y);
        }

        public static Fraction2 operator *(Fraction2 v, Fraction f)
        {
            return new Fraction2(v.x * f, v.y * f);
        }

        public static Fraction2 operator /(Fraction2 v, Fraction f)
        {
            return new Fraction2(v.x / f, v.y / f);
        }

        public static explicit operator float3(Fraction2 v)
        {
            return new float3((float)v.x, 0.0f, (float)v.y);
        }

        public static explicit operator Fraction2(float3 v)
        {
            return new Fraction2((Fraction)v.x, (Fraction)v.z);
        }

        public static explicit operator Vector3(Fraction2 v)
        {
            return new Vector3((float)v.x, 0.0f, (float)v.y);
        }

        public static explicit operator Fraction2(Vector3 v)
        {
            return new Fraction2((Fraction)v.x, (Fraction)v.z);
        }

        public override string ToString()
        {
            return $"({x}, {y})";
        }
    }
}

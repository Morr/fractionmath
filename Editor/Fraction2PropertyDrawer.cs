using UnityEditor;
using UnityEngine;
using Fractions;

namespace FractionsEditor
{
    [CustomPropertyDrawer(typeof(Fraction2))]
    public class Fraction2PropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            float labelWidth = 12.0f;
            float spacing = 5.0f;
            float fieldWidth = (position.width - (labelWidth * 2.0f) - spacing) / 2.0f;
            float offset = position.x;

            Rect xLabelRect = new Rect(offset, position.y, labelWidth, position.height);
            offset += labelWidth;

            Rect xFieldRect = new Rect(offset, position.y, fieldWidth, position.height);
            offset += fieldWidth;
            offset += spacing;

            Rect yLabelRect = new Rect(offset, position.y, labelWidth, position.height);
            offset += labelWidth;

            Rect yRect = new Rect(offset, position.y, fieldWidth, position.height);


            EditorGUI.LabelField(xLabelRect, "X");
            EditorGUI.PropertyField(xFieldRect, property.FindPropertyRelative("x"), GUIContent.none, true);
            EditorGUI.LabelField(yLabelRect, "Y");
            EditorGUI.PropertyField(yRect, property.FindPropertyRelative("y"), GUIContent.none, true);

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}

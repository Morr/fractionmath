using UnityEditor;
using UnityEngine;
using Fractions;

namespace FractionsEditor
{
    [CustomPropertyDrawer(typeof(Fraction))]
    public class FractionPropertyDrawer : PropertyDrawer
    {
        private SerializedProperty numeratorProperty;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            Rect numeratorRect = new Rect(position.x, position.y, position.width, position.height);
            numeratorProperty = property.FindPropertyRelative("Numerator");
            string fractionStringValue = new Fraction(numeratorProperty.intValue).ToString();

            if (Fraction.TryParse(EditorGUI.TextField(numeratorRect, fractionStringValue), out Fraction parsedNumber) == true)
            {
                numeratorProperty.intValue = parsedNumber.Numerator;
            }

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}

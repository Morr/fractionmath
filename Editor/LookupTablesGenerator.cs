using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Fractions;

namespace FractionsEdtitor
{
    public static class LookupTablesGenerator
    {
        const string LOOKUP_TABLES_SCRIPT_PATH = "Packages/com.morr.fractionmath/Runtime/FractionMath.LookupTables.cs";

        [UnityEditor.MenuItem("Tools/Fraction Math/Generate Lookup Tables", false, 10)]
        private static void GenerateLookupTables()
        {
            string scriptContent = GetLookupTablesScript();
            SaveLookupTablesScript(scriptContent, true);
        }

        [UnityEditor.Callbacks.DidReloadScripts]
        private static void FillLookupTables()
        {
            if (FractionMath.IsCalculationModeEnabled == false)
            {
                return; 
            }

            string scriptContent = GetLookupTablesScript();
            Type fractionMathType = typeof(FractionMath);
            StringBuilder stringBuilder = new StringBuilder();
            IEnumerable<FieldInfo> lookupTablesFields = fractionMathType.GetFields(BindingFlags.NonPublic | BindingFlags.Static).Where(prop => prop.IsDefined(typeof(LookupTableAttribute), false));

            foreach (FieldInfo field in lookupTablesFields)
            {
                Debug.Log(field.Name);

                stringBuilder.Clear();
                Array array = field.GetValue(null) as Array;
                Type arrayElementType = array.GetType().GetElementType();
                stringBuilder.Append($"{ field.Name } = {{ ");

                foreach (object element in array)
                {
                    string elementString = GetElementString(element, arrayElementType);

                    if (elementString != null)
                    {
                        stringBuilder.Append($"{elementString}, ");
                    }
                }

                stringBuilder.Append(" };");

                Regex assignments = new Regex($"{field.Name} =.*;");
                scriptContent = assignments.Replace(scriptContent, stringBuilder.ToString(), 1);
            }

            SaveLookupTablesScript(scriptContent, false);
            Debug.Log("Lookup tables generation complete");
        }

        private static string GetElementString(object element, Type elementType)
        {
            string GetFractionString(Fraction fraction)
            {
                return $"new {typeof(Fraction).Name}({fraction.Numerator})";
            }

            if (elementType.Equals(typeof(Fraction)))
            {
                Fraction scalar = (Fraction)element;
                return GetFractionString(scalar);
            }
            else if (elementType.Equals(typeof(Fraction2)))
            {
                Fraction2 vector = (Fraction2)element;
                Fraction x = vector.x;
                Fraction y = vector.y;
                return $"new {typeof(Fraction2).Name}({GetFractionString(x)}, {GetFractionString(y)})";
            }

            return null;
        }

        private static string GetLookupTablesScript()
        {
            string scriptText = AssetDatabase.LoadAssetAtPath<TextAsset>(LOOKUP_TABLES_SCRIPT_PATH).text;
            return scriptText;
        }

        private static void SaveLookupTablesScript(string scriptContent, bool isCalculationModeEnabled)
        {
            string phrase = "#define CALCULATION_MODE";
            string commentedPhrase = $"//{phrase}";

            if (isCalculationModeEnabled == true)
            {
                scriptContent = Regex.Replace(scriptContent, commentedPhrase, phrase);
            }
            else
            {
                scriptContent = Regex.Replace(scriptContent, phrase, commentedPhrase);
            }

            File.WriteAllText(LOOKUP_TABLES_SCRIPT_PATH, scriptContent);
            AssetDatabase.ImportAsset(LOOKUP_TABLES_SCRIPT_PATH);
            AssetDatabase.SaveAssets();
        }
    }
}
